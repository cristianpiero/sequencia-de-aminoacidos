#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

# Nº de itens por arquivo
#num_per_page = 100

file = raw_input("Informe o nome do arquivo: ")
file = file.strip()


if  not os.path.isfile(file):
    print "o arquivo %s não existe"%file

folder = "%s-files"%file
if '.' in file:
    folder = file.split('.')[0]

if not os.path.isdir(folder):
    os.mkdir(folder)

x = 0

with open(file, "r") as ins:
    array = []
    for line in ins:
        if x >= 0:
            flag =  False
            line = line.strip()

            if not '>' in line:
                flag = True
                ln ="%s \n%s\n"%(l.strip(),line.strip())
            else:
                title_id = line.split('_')
                l = "_".join(title_id[:2])

            if flag:
                array.append(ln)

        x+=1

#setei para gerar somente uma pagina
num_per_page = len(array)

pages = len(array)/num_per_page
if len(array)%num_per_page > 0:
    pages+=1

for n in range(0,pages):
    num_file = n+1
    filename = "%s/%s.txt"%(folder, num_file)
    file = open(filename, 'w')

    pagination = n*num_per_page
    print u"Paginado sequências %s de %s ..."%(num_file,pages)
    for item in array[pagination:pagination+num_per_page]:
        file.writelines(item)

    file.close()

print "Total de sequencias: %s"%len(array)

